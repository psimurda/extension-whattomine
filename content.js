
var coinRows = document.getElementsByTagName("tbody")[0].children
var USD2CZK = 20.34
console.log(coinRows.length)
for(i = 0; i < coinRows.length; i++){
	var revAndProfit = coinRows[i].children[7] // 7 th TD is rev and profit in $

	if(typeof revAndProfit !== "undefined"){
		var profitUSD = revAndProfit.getElementsByTagName('strong')[0].innerHTML.trim()
		var CzkProfit = (parseFloat(profitUSD.substr(1)) * USD2CZK)
		
		var CzkElement = document.createElement("tr");
		CzkElement.innerHTML = "<strong>" + CzkProfit.toFixed(2) + " Kč</strong>"
		revAndProfit.append(CzkElement)

		var CzkElementMonth = document.createElement("tr");
		CzkElementMonth.innerHTML = "<strong>" + (CzkProfit*30).toFixed(2) + " Kč (month)</strong>"
		
		revAndProfit.append(CzkElement)
		revAndProfit.append(CzkElementMonth)

		console.log(CzkProfit)
	}
}